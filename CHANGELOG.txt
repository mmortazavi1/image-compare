## [WIP]
- Photometric transform
- Zoom result using mouse interaction on desktop

## [0.4.0] - 2022-08-03
- Allow users to choose the transformation (affine, perspective, thin plate spline)
- Change difference computation implementation to use the canvas compositing instead of WASM

## [0.3.0] - 2022-03-04
- Support landscape view mode
- Export Aligned Images as PNGs and the Toggle visualisation as a GIF.
- Images will occupy available free space.
- Clarify UI with icons, supporting text.
- Added Licenses
- Self hosting third party dependencies (materialui, gifshot, icons, fonts, etc.)

## [0.2.0] - 2022-01-28
- new visualisations: toggle, slider, overlay, hover, difference
- new usecases (spot the difference) and updates to existing usecases (painting, book)
- installation guide added to website
- google analytics to record software usage

## [0.1.0] - 2021-11-19
- 5 sample use cases (book, painting, photograph, satellite, music)
- Uses Affine transformation by default
- Includes toggle visualisation with 3 speeds
- Works on both desktop and mobile phone web browser
- Can be installed as an offline application (progressive web app) in mobile phones

